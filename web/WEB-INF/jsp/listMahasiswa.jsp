<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>
            List Mahasiswa
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css" type="text/css" />
    </head>
    <body>
        <table border="6"  bordercolor="#dfebff" width="80%" align="center">
            <tr align="center" class="title_column" >
                <td>NIM</td>
                <td>NAMA</td>
                <td>TANGGAL LAHIR</td>
                <td>JURUSAN</td>
                <td>EMAIL</td>
                <td>ALAMAT</td>
                <td colspan="2"> </td>
            </tr>

            <c:forEach var="mahasiswa" items="${mahasiswaList}">
                <tr align="center">
                    <td>${mahasiswa.nim}</td>
                    <td>${mahasiswa.nama}</td>
                    <td>${mahasiswa.tglLahir}</td>
                    <td>${mahasiswa.jurusan}</td>
                    <td>${mahasiswa.email}</td>
                    <td align="left" >${mahasiswa.alamat}</td>
                    <td colspan="2">
                        <a href="editMahasiswa.htm?nim=${mahasiswa.nim}">Edit</a>&nbsp;&nbsp;
                        <a href="deleteMahasiswa.htm?nim=${mahasiswa.nim}">Hapus</a>
                    </td>
                </tr>
            </c:forEach>
            <tr>
                <td colspan="8"><a href="addMahasiswa.htm">Daftar Mahasiswa</a></td>
            </tr>
        </table>
    </body>
</html>
