<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Tutorial SpringMVC</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css" type="text/css" />

        <link type="text/css" href="css/redmond/jquery-ui-1.8.11.custom.css" rel="Stylesheet" />
        <script type="text/javascript" src="js/jquery-1.5.1.min.js" ></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js" ></script>
        <script type="text/javascript">
            $(function() {
                $( "#tglLahir" ).datepicker({
                    changeYear: true,
                    changeMonth: true,
                    yearRange: '1970:2030'
                });
            });
        </script>
    </head>
    <body>
        <form:form commandName="mahasiswa" method="POST">
            <div class="input text">
                <label>Nim</label>
                <form:input type="text" path="nim" value="${mahasiswa.nim}" />
                <font color="red" ><form:errors path="nim" cssClass="error" /></font>
            </div>
            <div class="input text">
                <label>Nama</label>
                <form:input type="text" path="nama" value="${mahasiswa.nama}" />
                <font color="red" ><form:errors path="nama" cssClass="error" /></font>
            </div>
            <div class="input text">
                <label>Tanggal Lahir</label>
                <form:input id="tglLahir" type="text" path="tglLahir" value="${mahasiswa.tglLahir}" />
            </div>
            <div class="input text">
                <label>Jurusan</label>
                <form:input type="text" path="jurusan" value="${mahasiswa.jurusan}" />
                <font color="red" ><form:errors path="jurusan" cssClass="error" /></font>
            </div>
            <div class="input text">
                <label>Email</label>
                <form:input type="text" path="email" value="${mahasiswa.email}" />
                <font color="red" ><form:errors path="email" cssClass="error" /></font>
            </div>
            <div class="input text">
                <label>Alamat</label>
                <form:input type="text" path="alamat" value="${mahasiswa.alamat}" />
                <font color="red" ><form:errors path="alamat" cssClass="error" /></font>
            </div>
            <div class="input submit">
                <input type="submit" value="Submit" />
            </div>
            <div>
                <a href="listMahasiswa.htm">List Mahasiswa</a>
            </div>
        </form:form>
    </body>
</html>
