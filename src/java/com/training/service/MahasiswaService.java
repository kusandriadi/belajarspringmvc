/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.training.service;

import com.training.entity.Mahasiswa;
import java.util.List;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Kus Andriadi
 */
@Repository
@Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
public class MahasiswaService {
    
   private final Logger log = LoggerFactory.getLogger(MahasiswaService.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional(readOnly = false)
    public void delete(Mahasiswa mahasiswa) {
        log.debug("Delete nim : {}", mahasiswa.getNim());
        sessionFactory.getCurrentSession().delete(mahasiswa);
    }

    @Transactional(readOnly = false)
    public void saveOrUpdate(Mahasiswa mahasiswa) {
        log.debug("Update nim : {}", mahasiswa.getNim());
        sessionFactory.getCurrentSession().saveOrUpdate(mahasiswa);
    }

    public List<Mahasiswa> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Mahasiswa").list();
    }

    public Mahasiswa getNim(String nim) {
        log.debug("Cari nim : {}", nim);
        return (Mahasiswa) sessionFactory.getCurrentSession().createQuery("from Mahasiswa where nim = :nim").
                setParameter("nim", nim).uniqueResult();
    }
}
