/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.training.controller;

import com.training.entity.Mahasiswa;
import com.training.service.MahasiswaService;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Kus Andriadi
 */
@Controller
public class MahasiswaController {
    
    @Autowired
    private MahasiswaService mahasiswaService;

    @RequestMapping(value = {"/addMahasiswa", "/editMahasiswa"}, method = RequestMethod.GET)
    private ModelMap getMahasiswa(@RequestParam(value = "nim", required = false) String nim) throws ParseException {
        Mahasiswa mahasiswa;
        if (nim == null) {
            mahasiswa = new Mahasiswa();
        } else {
            mahasiswa = mahasiswaService.getNim(nim);
        }
        return new ModelMap(mahasiswa);
    }

    @RequestMapping(value = "/addMahasiswa", method = RequestMethod.POST)
    private String saveMahasiswa(@ModelAttribute @Valid Mahasiswa mahasiswa, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "addMahasiswa";
        }
        mahasiswaService.saveOrUpdate(mahasiswa);
        return "redirect:listMahasiswa.htm";
    }

    @RequestMapping(value = "/listMahasiswa", method = RequestMethod.GET)
    private ModelMap getAll(@ModelAttribute Mahasiswa mahasiswa) {
        List<Mahasiswa> mahasiswaList = mahasiswaService.getAll();
        return new ModelMap("mahasiswaList", mahasiswaList);
    }

    @RequestMapping(value = "/deleteMahasiswa", method = RequestMethod.GET)
    private String deleteMahasiswa(@ModelAttribute Mahasiswa mahasiswa) {
        mahasiswaService.delete(mahasiswa);
        return "redirect:listMahasiswa.htm";
    }

    @RequestMapping(value = "/editMahasiswa", method = RequestMethod.POST)
    private String editMahasiswa(@ModelAttribute @Valid Mahasiswa mahasiswa, BindingResult bindingResult) throws ParseException {
        if (bindingResult.hasErrors()) {
            return "editMahasiswa";
        }
        mahasiswaService.saveOrUpdate(mahasiswa);
        return "redirect:listMahasiswa.htm";
    }
}
