/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.training.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Kus Andriadi
 */
@Entity
public class Mahasiswa {

    @Id
    @NotEmpty
    @Length(max=10)
    private String nim;
    
    @Length(max=250)
    @NotEmpty
    private String nama;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglLahir;
    
    @Length(max=20)
    @NotEmpty
    private String jurusan;
    
    @Length(max=50)
    @NotEmpty
    @Email
    private String email;
    
    private String alamat;

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public Date getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(Date tglLahir) {
        this.tglLahir = tglLahir;
    }
}
